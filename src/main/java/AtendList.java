﻿import java.util.ArrayList;
import java.util.List;

public class AtendList {

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("Przemysław Bykowski");
	    list.add("Marek Roszman");
        list.add("Adam Różański");
        list.add("Kamil PAul");
        list.add("Mateusz Myśliński");
        list.add("Igor Stencel");
        list.add("Marcin Stach");
        list.add("Damian Szarało");
        list.add("Patryk Murawski");
        list.add("Piotr Okrój");
        list.add("Katarzyna Polońska");
        list.add("Kamil Pepliński");
        list.add("Michał Okulski");
        list.add("Paweł Celiński");
        list.add("Bartosz \"Suseł\" Rutkowski ");
        System.out.println("Kto jest obecny:");
        for (String s : list) {
            System.out.println(s);
        }

    }
}
